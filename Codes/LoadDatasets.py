'''
Created on 10/05/2018

Auxiliary library to load and configure some datasets

@author: Carlos Alexandre Siqueira da Silva
'''
import numpy as np

def LoadDataset(DatasetName):
    TestSet = []

    if DatasetName == "Isolet": # 7797 samples
        pathDataset = "Datasets/Isolet/isolet_train.csv"
        TrainSet = np.matrix(np.loadtxt(pathDataset, delimiter=','))
        pathDataset = "Datasets/Isolet/isolet_test.csv"
        TestSet = np.matrix(np.loadtxt(pathDataset, delimiter=','))
        TrainSet = np.hstack((TrainSet[:, -1]-1, TrainSet[:, :-1]))
        TestSet = np.hstack((TestSet[:, -1]-1, TestSet[:, :-1]))

        numIn = 617
        numOut = 26 # Classes

    if DatasetName == "Spam": #4601 samples
        pathDataset = "Datasets/Spam/spam.csv"
        TrainSet = np.matrix(np.loadtxt(pathDataset, delimiter=','))
        TrainSet = np.hstack((TrainSet[:, -1], TrainSet[:, :-1]))

        for i in range(1, TrainSet.shape[1]):
            TrainSet[:,i] = (TrainSet[:,i] - TrainSet[:,i].min())/(TrainSet[:,i].max() - TrainSet[:,i].min())

        numIn = 57
        numOut = 2 # Classes

    if DatasetName == "MNIST": #70000 samples
        pathDataset = "Datasets/MNIST/mnist_train"
        TrainSet = np.matrix(np.loadtxt(pathDataset, delimiter=','))
        pathDataset = "Datasets/MNIST/mnist_test"
        TestSet = np.matrix(np.loadtxt(pathDataset, delimiter=','))

        TrainSet[:,1:] = TrainSet[:,1:]/255.0
        TestSet[:,1:] = TestSet[:,1:]/255.0
        
        numIn = 784
        numOut = 10

    if DatasetName == "DNA": #3186 samples
        pathDataset = "Datasets/DNA/dna_train.csv"
        BaseTemp = np.matrix(np.loadtxt(pathDataset, delimiter=','))
        pathDataset = "Datasets/DNA/dna_val.csv"
        TrainSet = np.matrix(np.loadtxt(pathDataset, delimiter=','))
        TrainSet = np.vstack((BaseTemp, TrainSet))

        pathDataset = "Datasets/DNA/dna_test.csv"
        TestSet = np.matrix(np.loadtxt(pathDataset, delimiter=','))

        TrainSet = np.hstack((TrainSet[:, 0:1]-1, TrainSet[:, 1:]))
        TestSet = np.hstack((TestSet[:, 0:1]-1, TestSet[:, 1:]))

        numIn = 180
        numOut = 3 # Classes

    if DatasetName == "Gisette": #7000 samples
        pathDataset = "Datasets/Gisette/gisette_train.csv"
        TrainSet = np.matrix(np.loadtxt(pathDataset, delimiter=','))
        pathDataset = "Datasets/Gisette/gisette_valid.csv"
        TestSet = np.matrix(np.loadtxt(pathDataset, delimiter=','))
        
        TrainSet = np.hstack((TrainSet[:, -1]+1, TrainSet[:, :-1]))
        TestSet = np.hstack((TestSet[:, -1]+1, TestSet[:, :-1]))

        # According to UCI tech report, data in this dataset was quantized to 1000 levels.
        TrainSet[:, 1:] = TrainSet[:, 1:]/1000
        TestSet[:, 1:] = TestSet[:, 1:]/1000

        numIn = 5000
        numOut = 3 # Base tem apenas 2 saidas, mas como os valores sao -1 e 1, configuro 3 saidas

    if DatasetName == "COIL20": #1440 samples
        pathDataset = "Datasets/COIL20/COIL20"
        TrainSet = np.matrix(np.loadtxt(pathDataset, delimiter=','))

        numIn = 1024
        numOut = 20 # Classes

    if DatasetName == "COIL100": #7200 samples
        pathDataset = "Datasets/COIL100/COIL100"
        TrainSet = np.matrix(np.loadtxt(pathDataset, delimiter=','))

        numIn = 1024
        numOut = 100 # Classes

    if DatasetName == "CrowdMap":#"CrowdsourcedMapping": # 10546 + 300 samples
        pathDataset = "Datasets/CrowdsourcedMapping/training.csv"
        TrainSet = np.matrix(np.loadtxt(pathDataset, delimiter=','))
        pathDataset = "Datasets/CrowdsourcedMapping/testing.csv"
        TestSet = np.matrix(np.loadtxt(pathDataset, delimiter=','))

        for i in range(1, TrainSet.shape[1]):
            # Normalizo treino e teste pela base de treino
            TrainSet[:,i] = (TrainSet[:,i] - TrainSet[:,i].min())/(TrainSet[:,i].max() - TrainSet[:,i].min())
            TestSet[:,i] = (TestSet[:,i] - TrainSet[:,i].min())/(TrainSet[:,i].max() - TrainSet[:,i].min())

        numIn = 28
        numOut = 6 # Classes

    if DatasetName == "KDEF_Front": #980 samples
        pathDataset = "Datasets/KDEF/BaseKDEF_30_Frente"
        TrainSet = np.matrix(np.loadtxt(pathDataset, delimiter=','))
        TrainSet = TrainSet[:, 1:]

        numIn = 900
        numOut = 7 # Classes

    if DatasetName == "StatlogImgSeg": #2310 samples
        pathDataset = "Datasets/Statlog_ImageSegmentation/segment.dat"
        TrainSet = np.matrix(np.loadtxt(pathDataset, delimiter=','))
        TrainSet = np.hstack((TrainSet[:, -1]-1, TrainSet[:, :-1]))

        for i in range(1, TrainSet.shape[1]):
            minVal = TrainSet[:,i].min()
            maxVal = TrainSet[:,i].max()
            if (maxVal - minVal) > 0:
                TrainSet[:,i] = (TrainSet[:,i] - TrainSet[:,i].min())/(TrainSet[:,i].max() - TrainSet[:,i].min())
            else:
                TrainSet[:,i] = 0

        numIn = 19
        numOut = 7 # Classes

    if DatasetName == "StatlogLandSat": #6435 samples
        pathDataset = "Datasets/Statlog_LandsatSatellite/sat.trn"
        TrainSet = np.matrix(np.loadtxt(pathDataset, delimiter=' '))
        TrainSet = np.hstack((TrainSet[:, -1]-1, TrainSet[:, :-1]))
        pathDataset = "Datasets/Statlog_LandsatSatellite/sat.tst"
        TestSet = np.matrix(np.loadtxt(pathDataset, delimiter=' '))
        TestSet = np.hstack((TestSet[:, -1]-1, TestSet[:, :-1]))

        TrainSet[:,1:] = TrainSet[:,1:]/255.0
        TestSet[:,1:] = TestSet[:,1:]/255.0

        numIn = 36
        numOut = 7 # Classes

    if DatasetName == "Musk": #6598 samples
        pathDataset = "Datasets/Musk/musk.csv"
        TrainSet = np.matrix(np.loadtxt(pathDataset, delimiter=','))
        TrainSet = TrainSet[:, :-1] # Classe esta repetida na ultima coluna

        for i in range(1, TrainSet.shape[1]):
            TrainSet[:,i] = (TrainSet[:,i] - TrainSet[:,i].min())/(TrainSet[:,i].max() - TrainSet[:,i].min())

        numIn = 166 # Base tem 168 entradas, mas as duas primeiras sao textuais e foram descartadas
        numOut = 2 # Classes

    if DatasetName == "Waveform": #5000 samples
        pathDataset = "Datasets/Waveform/waveform.data"
        TrainSet = np.matrix(np.loadtxt(pathDataset, delimiter=','))
        TrainSet = np.hstack((TrainSet[:, -1], TrainSet[:, :-1]))

        for i in range(1, TrainSet.shape[1]):
            TrainSet[:,i] = (TrainSet[:,i] - TrainSet[:,i].min())/(TrainSet[:,i].max() - TrainSet[:,i].min())

        numIn = 21
        numOut = 3 # Classes


    return TrainSet, TestSet, numIn, numOut
