'''
Created on 30/07/2018

This class implements some TensorFlow operations of the SSOE-ELM and SSOE-FP-ELM algorithms.

@author: Carlos Alexandre Siqueira da Silva
'''
from __future__ import print_function
import tensorflow as tf
import numpy as np
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'  # Disable tensorflow messages

class OPER_TF(object):
    def __init__(self):

        # Limit size for block decomposition in matrix inversion.
        self.limInv = 1000

        # Executo uma operacao inicial, pois primeira chamada a GPU eh mais lenta 
        self.TFMatrixMult(np.random.rand(5, 5), np.random.rand(5, 5))


    def CalcH_sig(self, MatX, MatW):
        # H matrix computation, with sigmoid activation function.
        return tf.nn.sigmoid(tf.matmul(MatX, MatW))


    def TFMatrixMult(self, Mat1, Mat2, transp1=False, transp2=False):
        # Matrix multiplication using TensorFlow (for performance reasons)
        return tf.matmul(Mat1, Mat2, transpose_a=transp1, transpose_b=transp2)


    def TFMatrixInversion(self, Mat1):
        # Matrix inversion using TensorFlow (for performance reasons)
        try:
            res = tf.linalg.inv(Mat1)
        except Exception as e:
            print("Erro: ", e)
            res = np.zeros(Mat1.shape)
        return res
        
    
    def MatrixInversion(self, Mat1):
        # Block Matrix Decomposition, to accelerate huge matrices inversion
        '''
        Inversao da matriz U usando Block Matrix Decomposition and Matrix Inversion Lemma
         
        HtH^{-1}  =  U^{-1}  =  [A  B]^{-1}  =  [P1  P2]
                                [C  D]          [P3  P4]
         
        P1 =  (A - BD^{-1}C)^{-1}
        P2 = -(A - BD^{-1}C)^{-1}BD^{-1}
        P3 = -D^{-1}C(A - BD^{-1}C)^{-1}
        P4 =  D^{-1}+D^{-1}C(A - BD^{-1}C)^{-1}BD^{-1}
 
        Para melhorar performance, preciso isolar
        Q1 = D^{-1}
        Q2 = (A - B.Q1.C)^{-1}
 
        P1 =  Q2
        P2 = -Q2.B.Q1
        P3 = -Q1.C.Q2
        P4 =  Q1 + Q1.C.Q2.B.Q1
        '''
        if Mat1.shape[0] <= self.limInv:
            # TensorFlow matrix inversion
            res = self.TFMatrixInversion(Mat1)
        else:
            # Recursive matrix decomposition
            res = np.zeros(Mat1.shape)
            dimMat = Mat1.shape[0] // 2
             
            matA = Mat1[:dimMat, :dimMat]
            matB = Mat1[:dimMat, dimMat:]
            matC = Mat1[dimMat:, :dimMat]
            matD = Mat1[dimMat:, dimMat:]
 
            matQ1 = self.MatrixInversion(matD)
            Q2Temp = self.TFMatrixMult(self.TFMatrixMult(matB, matQ1), matC)
            matQ2 = self.MatrixInversion(matA - Q2Temp)
            res[:dimMat, :dimMat] = matQ2 #P1
            Q2Temp = []
             
            matP2 = self.TFMatrixMult(self.TFMatrixMult(matQ2, matB), matQ1) * (-1)
            res[:dimMat, dimMat:] = matP2
            matP2 = []
             
            P3Temp = self.TFMatrixMult(self.TFMatrixMult(matQ1, matC), matQ2)
            res[dimMat:, :dimMat] = P3Temp * (-1) #P3
 
            P4Temp = self.TFMatrixMult(self.TFMatrixMult(P3Temp, matB), matQ1)
            P3Temp = []
            res[dimMat:, dimMat:] = matQ1 + P4Temp #P4
            P4Temp = []
             
        return res


    def CalcMatrixL(self, MatIn, numNeighbors, flagGauss=True):
        # Laplacian matrix L
        r1 = tf.reduce_sum(MatIn*MatIn, axis=1)
        r2 = tf.reshape(r1, [-1, 1])
        dists = r2 - 2*tf.matmul(MatIn, MatIn, transpose_b=True) + tf.transpose(r2)
        distsNeg = tf.negative(dists)
        
        # distNeg -> NEGATIVO DAS DISTANCIAS DE TODOS OS ELEMENTOS DA MATRIZ DE ENTRADA (MATRIZ DENSA)
        #            OBS.: RETORNA O QUADRADO DAS DISTANCIAS.
        
        values, indices = tf.nn.top_k(distsNeg, k=numNeighbors, sorted=False)
        valores = tf.ones((tf.shape(values)[0]*tf.shape(values)[1],), dtype=tf.float64)
        replicated_first_indices = tf.tile(
            tf.expand_dims(tf.range(tf.shape(indices)[0]), axis=1),
            [1, tf.shape(indices)[1]])
        pack_mat = tf.stack([replicated_first_indices, indices], axis=2)
        pack_mat2 = tf.reshape(pack_mat, (tf.shape(indices)[0]*tf.shape(indices)[1], 2))
        
        matW1 = tf.scatter_nd(pack_mat2, valores, 
                              ([tf.shape(indices)[0], tf.shape(indices)[0]])) - tf.eye(tf.shape(MatIn)[0], dtype=tf.float64)
        matWGauss = tf.multiply(tf.exp(distsNeg / 2), matW1)

        # matW1 -> MATRIZ ESPARSA COM "1" NOS K-VIZINHOS DE CADA INSTANCIA.
        # matWGauss -> MATRIZ ESPARSA COM FUNCAO GAUSSIANA NOS K-VIZINHOS DE CADA INSTANCIA.
        #              { f = exp(-||xi - xj||^2 / 2sigma^2) }
        
        # Lap = D - W
        matDiag1 = tf.linalg.diag(tf.reduce_sum(matW1, axis=1))
        matLap1 = matDiag1 - matW1

        matDiagGauss = tf.linalg.diag(tf.reduce_sum(matWGauss, axis=1))
        matLapGauss = matDiagGauss - matWGauss

        if flagGauss:
            return matLapGauss, matDiagGauss
        else:
            return matLap1, matDiag1

    
    def CalcMatrixJ(self, MatOut, paramJ):
        # Penalty coefficient matrix J
        C1 = tf.linalg.diag(tf.reduce_sum(MatOut, 0))
        C2 = tf.matmul(MatOut, C1)
        J = tf.linalg.diag(paramJ / tf.reduce_sum(C2, 1))
        resMatJ = tf.where(tf.math.is_inf(J), tf.zeros_like(J), J)

        return resMatJ
