'''
Created on 30/07/2018
SSOE-ELM: Semi-Supervised Online Elastic ELM
and
SSOE-FP-ELM: Semi-Supervised Online Elastic ELM with Forgetting Parameter

This class implements the SSOE-ELM and the SSOE-FP-ELM algorithms.

@author: Carlos Alexandre Siqueira da Silva
'''
from __future__ import print_function
import numpy as np
from scipy.stats import ranksums
import OPER_TF
import Utils

class SSOE_FP_ELM(object):
    def __init__(self, numDataIn, numNeurons, numDataOut, paramAlpha, paramC, paramMu):

        # OPER_TF is an auxiliary class of operations performed in TensorFlow.
        self.operTF = OPER_TF.OPER_TF()
        
        self.inputLen = numDataIn
        self.hiddenNum = numNeurons
        self.outputLen = numDataOut

        self.paramAlpha = paramAlpha
        self.paramC = paramC
        self.numNeighbors = paramMu
        
        self.BetaUpdated = False

        self.lastAccuracy = -1.0
        self.lastTrainingPart = []


    def InitMatrices(self, initWeights=True):
        # Initialization of all matrices: W, U and V.
        np.random.seed()
        
        if initWeights:
            self.W = np.random.rand(self.inputLen+1, self.hiddenNum) # Add Bias (+1) to inputLen
            self.W = np.matrix(self.W)
            self.W = (self.W * 2.0) - 1

        self.U = np.zeros((self.hiddenNum, self.hiddenNum))
        self.V = np.zeros((self.hiddenNum, self.outputLen))
        self.BetaUpdated = False


    def Train(self, LabTrainSet_In=[], LabTrainSet_Out=[], UnlabTrainSet=[], MatH=[], paramF=0.0):
        # Incremental training. If using SSOE-ELM, paramF does not need to be informed.
        # If usinf SSOE-FP-ELM, there is a method below to calculate the Forgetting Parameter.
        forgParam = 1.0 - paramF

        # Lambda computation
        if UnlabTrainSet != []:
            paramLambda = UnlabTrainSet.shape[0] / (UnlabTrainSet.shape[0] + LabTrainSet_In.shape[0])
        else:
            paramLambda = 0.0

        if LabTrainSet_In != []: #Has labeled samples
            DataIn = LabTrainSet_In
            if LabTrainSet_Out.shape[1] == 1:
                DataOut = Utils.SVet_SMat(LabTrainSet_Out, LabTrainSet_Out.shape[0], self.outputLen)
            else:
                DataOut = LabTrainSet_Out
        else:
            DataIn = np.zeros((0, self.inputLen))
            DataOut = np.zeros((0, self.outputLen))

        if UnlabTrainSet != []: #Has unlabeled samples
            DataIn = np.vstack((DataIn, UnlabTrainSet))
            DataOut = np.vstack((DataOut, np.zeros((UnlabTrainSet.shape[0], DataOut.shape[1]))))

        if MatH != []: #Has a previous H matrix
            MatHTemp = MatH
        else:
            DataInBias = np.hstack((DataIn, np.ones((DataIn.shape[0], 1)))) # Add Bias (+1) to inputLen
            MatHTemp = self.operTF.CalcH_sig(DataInBias, self.W)
            DataInBias = []
        
        matJ = self.operTF.CalcMatrixJ(DataOut, self.paramC)
        if(self.numNeighbors <= DataIn.shape[0]):
            matL, _ = self.operTF.CalcMatrixL(DataIn, self.numNeighbors)
        else:
            matL, _ = self.operTF.CalcMatrixL(DataIn, DataIn.shape[0])

        temp1 = self.operTF.TFMatrixMult(MatHTemp, matJ + matL*paramLambda, transp1=True)
        temp1 = self.operTF.TFMatrixMult(temp1, MatHTemp)
        self.U = (forgParam * self.U) + temp1 #Updating matrix U
        temp1 = []
        temp1 = self.operTF.TFMatrixMult(MatHTemp, matJ, transp1=True)
        temp1 = self.operTF.TFMatrixMult(temp1, DataOut)
        self.V = (forgParam * self.V) + temp1 #Updating matrix V
        temp1 = []

        self.BetaUpdated = False

        return MatHTemp


    def CalcBetas(self):
        #Beta computation is performed only if Beta needs to be recalculated.
        if not self.BetaUpdated:
            U_Ident = self.U + np.identity(self.hiddenNum)*self.paramAlpha
            U_Ident = self.operTF.MatrixInversion(U_Ident)
            self.Beta = self.operTF.TFMatrixMult(U_Ident, self.V)
            U_Ident = []
            self.BetaUpdated = True


    def Test(self, TestSet_In, TestSet_Out=[], MatH=[]):

        if MatH != []: #Has a previous H matrix
            MatHTemp = MatH
        else:
            DataIn = np.hstack((TestSet_In, np.ones((TestSet_In.shape[0], 1)))) # Add Bias (+1) to inputLen
            MatHTemp = self.operTF.CalcH_sig(DataIn, self.W)
        MatFX = self.operTF.TFMatrixMult(MatHTemp, self.Beta)

        if TestSet_Out == []:
            VetFX = np.reshape(np.argmax(MatFX, axis=1), (-1, 1))
            return VetFX
        else:
            DataOut = TestSet_Out
            if TestSet_Out.shape[1] > 1:
                DataOut = np.argmax(DataOut, axis=1)

            VetFX = np.reshape(np.argmax(MatFX, axis=1), (-1, 1))
            
            acur, acer, tam = Utils.accuracyClassif(DataOut, VetFX)
            return acur, acer, tam, VetFX


    def CalcLaplacianScore(self, TrainSet):
        matL, matD = self.operTF.CalcMatrixL(TrainSet, self.numNeighbors)
        
        LapScores = np.zeros((1, TrainSet.shape[1]))
        for i in range(TrainSet.shape[1]):
            
            num1 = np.sum(np.dot(np.transpose(TrainSet[:, i:i+1]), matD))
            den1 = np.sum(matD)
            F_hat = TrainSet[:, i:i+1] - (num1 / den1)
        
            num2 = (np.dot(np.dot(np.transpose(F_hat), matL), F_hat))[0, 0]
            den2 = (np.dot(np.dot(np.transpose(F_hat), matD), F_hat))[0, 0]
            LapScores[0, i] = num2 / np.max([den2, 1e-12])
        
        return LapScores
        

    def CalcForgettingParameter(self, LabTrainSet, FullTrainSet):
        tolAccurLab = 0.0
        tolDistInput = 0.0
        
        forgParamDinam1 = 0
        # Label Concept Drift

        self.CalcBetas() # Preciso calcular Betas a cada nova particao de treino

        acur1, _, _, _ = self.Test(LabTrainSet[:, 1:], LabTrainSet[:, 0:1])

        if self.lastAccuracy != -1:
            # Not the first partition...
            if acur1 < (self.lastAccuracy - tolAccurLab):
                # Drop in accuracy. Possibly a LCD occurred...
                forgParamDinam1 = self.lastAccuracy - acur1
        self.lastAccuracy = acur1

        forgParamDinam2 = 0
        # Feature Concept Drift

        LapScores = self.CalcLaplacianScore(FullTrainSet[:, 1:])
        # LapScores: lower values means more important features.
        # Reversed LapScores: higher values means more important features.
        LapScores = 1 - (LapScores / np.max(LapScores))
        SumLapScores = np.sum(LapScores)
        
        if self.lastTrainingPart != []:
            # Not the first partition...
            numDifFeat = 0
            for col in range(self.inputLen):
                data1 = np.ravel(self.lastTrainingPart[:, col+1])
                data2 = np.ravel(FullTrainSet[:, col+1])
                _, pvalue = ranksums(data1, data2) # Nonparametric statistical test
                if pvalue < 0.01:
                    # Significant changes in probabilistic distribution. Possibly a FCD occurred...
                    numDifFeat += LapScores[0, col]

            percDifFeat = numDifFeat / SumLapScores
            if percDifFeat > tolDistInput:
                forgParamDinam2 = percDifFeat

        self.lastTrainingPart = FullTrainSet
        
        # Hybrid Forgetting Parameter
        forgParamDinam = np.max([forgParamDinam1, forgParamDinam2])

        return forgParamDinam
