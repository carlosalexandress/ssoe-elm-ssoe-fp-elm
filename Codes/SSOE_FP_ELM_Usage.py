'''
Created on 15/01/2020

Simple usage guide of SSOE-ELM and SSOE-FP-ELM algorithms

@author: Carlos Alexandre Siqueira da Silva
'''
import numpy as np
import time
import SSOE_FP_ELM_TF
import LoadDatasets

def ExecTestSSOEFPELM(pDataset, numExecs, batchsize, percLab, paramAlpha,
                      paramC, paramMu, numNeur, dynamicFP):
    # The dynamicFP parameter indicates if the SSOE-ELM or the SSOE-FP-ELM will be used.
    # The difference between the two algorithms is the forgetting parameter calculation
    # performed before incremental training.
    
    TrainSetPre, TestSetPre, numIn, numOut = LoadDatasets.LoadDataset(pDataset)

    listAccuracy = np.zeros((numExecs, 1))
    listTrainTime = np.zeros((numExecs, 1))

    # Creating classifier
    ssoe_elm = SSOE_FP_ELM_TF.SSOE_FP_ELM(numIn, numNeur, numOut, paramAlpha, paramC, paramMu)

    if dynamicFP:
        Alg = "SSOE-FP-ELM"
    else:
        Alg = "SSOE-ELM"
    print("Algorithm: {}, Dataset: {}".format(Alg, pDataset))

    for ex in range(numExecs):

        # Separating training and test partitions
        TrainSet = np.copy(TrainSetPre)
        np.random.shuffle(TrainSet)
        if TestSetPre == []:
            numTrain = int(0.7*TrainSetPre.shape[0])
            TestSet = TrainSet[numTrain:, :]
            TrainSet = TrainSet[:numTrain, :]
        else:
            TestSet = np.copy(TestSetPre)

        # Init Matrices
        ssoe_elm.InitMatrices()


        t1 = time.time() # Training time measure

        # Training
        pInit = 0
        while pInit < TrainSet.shape[0]:

            pEnd = pInit+batchsize

            if pEnd + batchsize > TrainSet.shape[0]:
                #If last partition, adjust pEnd
                pEnd = TrainSet.shape[0]

            PartTemp = TrainSet[pInit:pEnd, :]
            
            numLab = int(PartTemp.shape[0]*percLab)

            forgParam = 0
            if dynamicFP:
                forgParam = ssoe_elm.CalcForgettingParameter(PartTemp[:numLab, :], PartTemp)

            ssoe_elm.Train(PartTemp[:numLab, 1:],
                           PartTemp[:numLab, 0:1],
                           PartTemp[numLab:, 1:],
                           paramF=forgParam)
            
            pInit = pEnd
    
        ssoe_elm.CalcBetas()
            
        listTrainTime[ex, 0] += (time.time() - t1) # Training time

        # Testing
        pInit = 0
        totAcer = 0
        totBase = 0
        while pInit < TestSet.shape[0]:
            pEnd = pInit+batchsize

            if pEnd + batchsize > TestSet.shape[0]:
                #If last partition, adjust pEnd
                pEnd = TestSet.shape[0]

            PartTemp = TestSet[pInit:pEnd, :]

            _, acer, tam, _ = ssoe_elm.Test(PartTemp[:, 1:], PartTemp[:, 0:1])
            totAcer += acer
            totBase += tam
            
            pInit = pEnd
        
        listAccuracy[ex, 0] = (totAcer / totBase)*100


        print("EExecution {:02} - Accuracy: {:.2f} % - Training Time: {:.2f} secs".format(ex+1,
                                                                                         listAccuracy[ex, 0],
                                                                                         listTrainTime[ex, 0]))
    
    print("Mean Accuracy: {:.2f} %, Std Accuracy: {:.2f} %".format(np.mean(listAccuracy), np.std(listAccuracy)))
    print("Mean Training Time: {:.2f}, Std Training Time: {:.2f}".format(np.mean(listTrainTime), np.std(listTrainTime)))

    del(ssoe_elm)

    return


if __name__ == '__main__':
    pDataset = "DNA"
    numExecs = 10
    batchsize = 200
    percLab = 0.5
    paramAlpha = int(2)**int(17)
    paramC = 10000.0
    paramMu = 5
    numNeur = 1350
    dynamicFP = False
    
    ExecTestSSOEFPELM(pDataset, numExecs, batchsize, percLab,
                      paramAlpha, paramC, paramMu, numNeur, dynamicFP)

