'''
Created on 17/06/2016

@author: Carlos Alexandre Siqueira da Silva
'''

from __future__ import division
import numpy as np

def SVet_SMat(pVec, numRows, numClasses):
    #Outputs in column-vector to matrix
    pMat = np.zeros((numRows, numClasses))
    for i in range(numRows):
        pMat[i, int(pVec[i])] = 1.0
    return pMat


def accuracyClassif(Obt, Esp):
    acer = 0
    tam = Obt.shape[0]
    i = 0
    for i in range(tam):
        if Obt[i, 0] == Esp[i, 0]:
            acer += 1
    return acer/tam, acer, tam
