# SSOE-ELM and SSOE-FP-ELM

## Semi-Supervised Online Elastic ELM and Semi-Supervised Online Elastic ELM with  Forgetting Parameter

---

On this repository you will find the implementation for SSOE-ELM and SSOE-FP-ELM algorithms.


## Usage guide:

The file "SSOE_FP_ELM_Usage.py" shows how to configure and execute the SSOE-ELM and SSOE-FP-ELM algorithms. The "LoadDatasets.py" file already contains settings for several public domain datasets. In the usage example, the DNA dataset was used, which is available in the LIBSVM repository https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/multiclass.html#dna. Other datasets can be downloaded from their respective repositories.


## Dependencies:

* Python 3.x
* NumPy
* Tensorflow 2
* Scikit-learn

## Citation

If you're using the SSOE-ELM or the SSOE-FP-ELM algorithms, please cite one of the following papers:

---

A more detailed paper on SSOE-ELM and SSOE-FP-ELM (Multimedia Tools and Applications, 2023):

```
@article{dasilva2023extreme,
  title={An extreme learning machine algorithm for semi-supervised classification of unbalanced data streams with concept drift},
  author={da Silva, Carlos Alexandre Siqueira and Krohling, Renato Antonio},
  journal={Multimedia Tools and Applications},
  pages={1--40},
  year={2023},
  publisher={Springer}
}
```

---

Original paper of SSOE-ELM (IJCNN, 2018):

```
@inproceedings{silva2018semisupervised,
	title={Semi-{S}upervised {O}nline {E}lastic {E}xtreme {L}earning {M}achine for {D}ata {C}lassification},
	author={da Silva, Carlos Alexandre Siqueira and Krohling, Renato Antonio},
	booktitle={2018 IEEE International Joint Conference on Neural Networks},
	pages={1511--1518},
	year={2018},
	organization={IEEE}
}
```

---

Original paper of SSOE-FP-ELM (IJCNN, 2019):

```
@inproceedings{silva2019semisupervised,
	title={Semi-{S}upervised {O}nline {E}lastic {E}xtreme {L}earning {M}achine with {F}orgetting {P}arameter to deal with concept drift in data streams},
	author={da Silva, Carlos Alexandre Siqueira and Krohling, Renato Antonio},
	booktitle={2019 IEEE International Joint Conference on Neural Networks},
	pages={1--8},
	year={2019},
	organization={IEEE}
}
```
